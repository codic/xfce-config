# xfce-config

My XFCE configuration. Compton configuration is at `compton.conf`.
I recommend using my theme (https://codeberg.org/gsbhasin84/Materiav2/src/branch/master/README.md) along with it